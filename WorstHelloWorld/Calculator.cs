﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WorstHelloWorld
{
    public class Calculator : WikiReader
    {
        private struct ExpectedPair
        {
            public char ExpectedChar;
            public int CharNum;
        }

        private ExpectedPair[] _indexes;

        public Calculator(SettingsBlob settings) : base(settings)
        {
        }

        public async Task Calculate()
        {
            try
            {
                Console.WriteLine();
                Console.WriteLine("Running Calculation:");
                if (string.IsNullOrWhiteSpace(_settings.ExpectedResult))
                {
                    Console.WriteLine("No expected result provided for calculation");
                    return;
                }

                string nextPage = ReaderConstants.StartAddress;
                using (_webClient = new WebClient())
                {
                    _webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    int resultLength = _settings.ExpectedResult.Length;
                    _indexes = new ExpectedPair[resultLength];

                    for (int i = 0; i < resultLength; i++)
                    {
                        _indexes[i] = new ExpectedPair() { ExpectedChar = _settings.ExpectedResult[i] };
                        nextPage = await ReadPage(nextPage, i);
                    }
                }

                await OutputFile();

                Console.WriteLine("Calculation Complete");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Got an exception:\n{ex.Message}\n({ex.ToString()})");
            }
        }

        private async Task<string> ReadPage(string pageUrl, int index)
        {
            ExpectedPair expected = _indexes[index];
            string fullAddress = $"{ReaderConstants.WikiDomain}{pageUrl}";
            Stream webData = _webClient.OpenRead(fullAddress);
            using (StreamReader webStream = new StreamReader(webData, Encoding.UTF8, false))
            {
                if (_settings.EnableDebugging) Console.Write($" Loading from {fullAddress}");
                string resultData = await webStream.ReadToEndAsync();
                if (_settings.EnableDebugging) Console.WriteLine($" Complete");

                string fileName = $"dump{index}.txt";
                if (_settings.EnableFileSaving)
                {
                    File.WriteAllText(fileName, resultData);
                }

                //  calculate a point between 25% and 50% to start searching through the file for the required character
                int resultLength = resultData.Length;
                int pageStart = new Random().Next((int)(resultLength * 0.25f), (int)(resultLength * 0.5f));
                char foundChar;
                bool firstPass = true;
                do
                {
                    foundChar = resultData[pageStart];
                    pageStart++;

                    if (pageStart >= resultLength)
                    {
                        if (firstPass)
                        {
                            Console.WriteLine("Unable to find character, jumping back to file start");
                            pageStart = 1;
                            firstPass = false;
                        }
                        else
                        {
                            break;
                        }
                    }
                } while (foundChar != expected.ExpectedChar);

                if (foundChar == expected.ExpectedChar)
                {
                    _indexes[index].CharNum = pageStart - 1;
                    Console.WriteLine($"Found \"{expected.ExpectedChar}\" at #{_indexes[index].CharNum}");

                    return GetNextURL(resultData.Substring(pageStart, 10000));
                }
                else
                {
                    Console.WriteLine($"Unable to find '{expected.ExpectedChar}' in {fullAddress}");
                    return string.Empty;
                }

            }
        }

        private async Task OutputFile()
        {
            if (!string.IsNullOrEmpty(_settings.InputPath))
            {
                StringBuilder s = new StringBuilder();
                bool isFirst = true;
                foreach (var item in _indexes)
                {
                    if (!isFirst)
                    {
                        if (_settings.EnableDebugging) Console.Write(ReaderConstants.IndexSeparator);
                        s.Append(ReaderConstants.IndexSeparator);
                    }

                    if (_settings.EnableDebugging) Console.Write(item.CharNum.ToString());
                    s.Append(item.CharNum.ToString());
                    isFirst = false;
                }
                if (_settings.EnableDebugging) Console.WriteLine();

                await File.WriteAllTextAsync(_settings.InputPath, s.ToString());
            }
        }
    }
}
