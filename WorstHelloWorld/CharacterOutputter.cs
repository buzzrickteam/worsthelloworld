﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WorstHelloWorld
{
    public class CharacterOutputter : WikiReader
    {
        private int[] ಠ_ಠ = { 84859, 105447, 13795, 24452, 43713, 120637, 122725, 96240, 44672, 76339, 88801, 53540, 42374 };
        private int _totalWebTraffic;

        public CharacterOutputter(SettingsBlob settingsBlob) : base(settingsBlob) { }

        public async Task RunOutput()
        {
            await LoadIndicies();
            try
            {
                int index = 0;
                DateTime startTime = DateTime.UtcNow;
                _totalWebTraffic = 0;
                string nextPage = ReaderConstants.StartAddress;
                using (_webClient = new WebClient())
                {
                    _webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    while (nextPage != string.Empty && index < ಠ_ಠ.Length)
                    {
                        nextPage = await ReadPage(nextPage, index);
                        index++;
                    }
                }
                TimeSpan totalTime = (DateTime.UtcNow - startTime);
                if (_settings.ShowStats)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Total web traffic = {_totalWebTraffic.ToString("N0")} bytes");
                    Console.WriteLine($"Total time taken = {totalTime.TotalSeconds.ToString("F2")} seconds");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Got an exception:\n{ex.Message}\n({ex.ToString()})");
            }
        }

        private async Task LoadIndicies()
        {
            if (!string.IsNullOrWhiteSpace(_settings.InputPath) && File.Exists(_settings.InputPath))
            {
                string indexesString = await File.ReadAllTextAsync(_settings.InputPath);
                string[] indexesList = indexesString.Split(new char[] { ReaderConstants.IndexSeparator });
                ಠ_ಠ = new int[indexesList.Length];

                if (_settings.EnableDebugging) Console.WriteLine("Indexes:");
                for (int i = 0; i < indexesList.Length; i++)
                {
                    if (int.TryParse(indexesList[i], out ಠ_ಠ[i]))
                    {
                        if (_settings.EnableDebugging) Console.Write($"{ಠ_ಠ[i]} ");
                        //ಠ_ಠ[i] = indexItem;
                    }
                }
            }
        }

        private async Task<string> ReadPage(string pageURL, int index)
        {
            int pageStart = ಠ_ಠ[index];
            string fullAddress = $"{ReaderConstants.WikiDomain}{pageURL}";
            if (_settings.EnableDebugging) Console.WriteLine($"Opening Webclient read from {fullAddress}");
            Stream webData = _webClient.OpenRead(fullAddress);
            using (StreamReader webStream = new StreamReader(webData, Encoding.UTF8, false))
            {
                if (_settings.EnableDebugging) Console.WriteLine($" Loading from {fullAddress}, starting at #{pageStart}");
                string resultData = await webStream.ReadToEndAsync();
                _totalWebTraffic += resultData.Length;

                string fileName = $"dump{index}.txt";
                if (_settings.EnableFileSaving)
                {
                    File.WriteAllText(fileName, resultData);
                }

                if (resultData.Length < pageStart)
                {
                    Console.WriteLine($"Unable to search content at character:{pageStart}. Page length is {resultData.Length}");
                    return string.Empty;
                }
                string foundCharacter = resultData.Substring(pageStart, 1);

                Console.Write(foundCharacter);
                string nextTen = resultData.Substring(pageStart + 1, 70);
                if (_settings.EnableForwardLook) Console.Write($" ...{nextTen}");
                if (_settings.EnableFileSaving) Console.Write($"({fileName})");
                if (_settings.EnableSourceURLs) Console.Write($" from {index} -{pageURL}");
                if (_settings.EnableForwardLook || _settings.EnableFileSaving || _settings.EnableSourceURLs)
                    Console.WriteLine();

                return GetNextURL(resultData.Substring(pageStart, 10000));
            }
        }
    }
}
