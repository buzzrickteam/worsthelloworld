﻿using System;
using System.Threading.Tasks;

namespace WorstHelloWorld
{
    class Program
    {

        static async Task Main(string[] args)
        {
            SettingsBlob settings = new SettingsBlob().ExtractSettings(args);

            if (settings.Calculate)
            {
                Calculator calculator = new Calculator(settings);
                await calculator.Calculate();
            }
            else
            {
                CharacterOutputter outputter = new CharacterOutputter(settings);
                await outputter.RunOutput();
            }

            Console.WriteLine("");
            Console.ReadKey();
        }
    }
}
