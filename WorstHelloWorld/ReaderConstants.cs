﻿namespace WorstHelloWorld
{
    public static class ReaderConstants
    {
        public const string WikiDomain = "https://en.wikipedia.org";
        public const string StartAddress = "/wiki/Procrastination";
        public const string HRefRegEx = @"href\s*=\s*(?:[""'](?<1>[^""']*)[""']|(?<1>\S+))";
        public const char IndexSeparator = ',';
    }
}
