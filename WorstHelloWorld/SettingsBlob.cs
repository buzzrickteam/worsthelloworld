﻿using System;
using System.IO;

namespace WorstHelloWorld
{
    public class SettingsBlob
    {
        public bool Calculate;
        public bool EnableDebugging;
        public bool EnableFileSaving;
        public bool EnableForwardLook;
        public bool EnableSourceURLs;
        public bool ShowStats;
        public string InputPath;
        public string ExpectedResult;

        public SettingsBlob ExtractSettings(string[] args)
        {
            //Console.WriteLine("Settings:");
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];
                //Console.WriteLine($"Arg:{arg}");

                if (arg.StartsWith("--"))
                {
                    arg = arg.Substring(2);

                    switch (arg.ToLowerInvariant())
                    {
                        case "c":
                        case "calc":
                        case "calculate":
                            Console.WriteLine("Calculation mode");
                            Calculate = true;
                            i++;
                            if (args.Length > i)
                            {
                                ExpectedResult = args[i];
                            }
                            else
                            {
                                Console.WriteLine("Missing \"Expected text\" value for Calculate parameter");
                            }
                            break;

                        case "d":
                        case "debug":
                            Console.WriteLine("Enable Debugging");
                            EnableDebugging = true;
                            break;

                        case "fs":
                        case "file":
                        case "dumpfiles":
                            Console.WriteLine("Enable File Saving");
                            EnableFileSaving = true;
                            break;

                        case "fl":
                        case "forward":
                            Console.WriteLine("Enable Forward Look");
                            EnableForwardLook = true;
                            break;

                        case "u":
                        case "urls":
                            Console.WriteLine("Enable Source URLs");
                            EnableSourceURLs = true;
                            break;

                        case "s":
                        case "showstats":
                        case "stats":
                            Console.WriteLine("Show Stats");
                            ShowStats = true;
                            break;

                        case "i":
                        case "input":
                            i++;
                            if (args.Length > i)
                            {
                                InputPath = Path.GetFullPath(args[i]);
                            }
                            else
                            {
                                Console.WriteLine("Missing \"filename\" text in Input parameter");
                            }
                            //Console.WriteLine($"InputPath:{InputPath}");
                            break;

                        case "string":
                            break;


                        case "h":
                        case "?":
                        case "help":
                            Console.WriteLine();
                            Console.WriteLine("WorstHelloWorld!");
                            Console.WriteLine("--h --help                           Help");
                            Console.WriteLine("--c --calculate [Expected Result]    Calculate updated indicies");
                            Console.WriteLine("--i --input                          Index input file");
                            Console.WriteLine("--s --stats                          Show Statistics");
                            Console.WriteLine("Debugging options:");
                            Console.WriteLine("--d --debug                          Show verbose debug logs");
                            Console.WriteLine("--fs --file --dump                   Dump downloaded content into indexed files");
                            Console.WriteLine("--fl --forward                       Output content forward looking");
                            Console.WriteLine("--u --urls                           Output urls");
                            Console.WriteLine();

                            break;

                        default:
                            Console.WriteLine($"Unhandled argument:{arg}");
                            break;


                    }
                }
                else
                {
                    Console.WriteLine($"Unhandled argument:{arg}");
                }
            }
            return this;
        }
    }
}
