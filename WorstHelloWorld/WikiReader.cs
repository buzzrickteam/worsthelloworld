﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WorstHelloWorld
{
    public class WikiReader
    {
        protected WebClient _webClient;
        protected Regex _regex = new Regex(ReaderConstants.HRefRegEx);
        protected SettingsBlob _settings;

        public WikiReader(SettingsBlob settings)
        {
            _settings = settings;
        }


        protected string GetNextURL(string dataBlob)
        {
            Match addressMatch = _regex.Match(dataBlob);
            while (addressMatch.Success)
            {
                string nextAddress = addressMatch.Value;
                nextAddress = nextAddress.Substring(6, nextAddress.Length - 7);
                if (nextAddress.Contains(":")       // skip offsite addresses and try again
                    || nextAddress.StartsWith("#")  //  skip in page anchor tags.
                    || nextAddress.StartsWith("//")  //  skip weirdly formatted offsite addresses
                    )
                {
                    addressMatch = addressMatch.NextMatch();
                    if (_settings.EnableDebugging) Console.WriteLine($"Found invalid Address: {nextAddress}, trying again");
                    continue;
                }
                if (_settings.EnableDebugging) Console.WriteLine($"Found next address = {nextAddress}");
                return nextAddress;
            }
            Console.WriteLine("End of file reached. Failing");
            return string.Empty;
        }

    }
}
